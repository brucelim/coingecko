const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * List all derivative tickers
 * 
 * @param {string} include_tickers - ['all', 'unexpired'] - expired to show unexpired tickers, all to list all tickers, defaults to unexpired
 * @returns {Promise<object[]>}
 */
const listDerivativeTickers = async (include_tickers) => {
    const params = { include_tickers }

    return await rpc(`derivatives`, filterparams(params))
}


/**
 * List all derivative exchanges
 * 
 * @param {object} [options]
 * @param {string|number} [options.order]
 * @param {string|number} [options.per_page]
 * @param {string|number} [options.page]
 * @returns {Promise<object[]>}
 */
const listDerivativeExchanges = async (options={}) => {
    const { order, per_page, page } = options
    const params = { order, per_page, page }

    return await rpc(`derivatives/exchanges`, filterparams(params))
}


/**
 * show derivative exchange data
 * 
 * @param {string|number} id
 * @param {string|number} include_tickers
 * @returns {Promise<object[]>}
 */
const getDerivativeExchange = async (id, include_tickers) => {
    const params = { include_tickers }

    return await rpc(`derivatives/exchanges/${id}`, filterparams(params))
}


/**
 * List all derivative exchanges name and identifier
 * 
 * @returns {Promise<object[]>}
 */
const listDerivativeExchangesNameIdentifier = async () => {
    return await rpc(`derivatives/exchanges/list`)
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        listDerivativeTickers,
        listDerivativeExchanges,
        getDerivativeExchange,
        listDerivativeExchangesNameIdentifier,
    }
}

module.exports = (params) => {
    return Object.fromEntries(Object.entries(params).filter(([_, v]) => v != null))
}
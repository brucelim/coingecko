'use strict'
require('isomorphic-fetch')
const fs = require('fs')

const sleep = ms => new Promise(r => setTimeout(r, ms));

module.exports = async function (method, params) {
    const base_url = 'https://api.coingecko.com/api/v3/'
    const query = new URLSearchParams(params).toString();
    const url = base_url + method + '?' + query
    const cache = './cache.json'
    let lastcall

    if (fs.existsSync(cache)) {
        const content = fs.readFileSync(cache)
        lastcall = JSON.parse(content)
    } else {
        lastcall = {time:+new Date()}
    }

    const wait = 5000
    const current = +new Date()
    if (lastcall.time + wait > current) {
        console.log(`sleeping: ${lastcall.time + wait - current} to prevent rate limiting, set shorter intervals in config`)
        await sleep(lastcall.time + wait - current)
    }

    console.log(url)
    fs.writeFileSync(cache, JSON.stringify({time:+new Date()}))
    return fetch(url)
        .then(res => res.json())
        .catch(error => error)
}


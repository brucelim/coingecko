const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * Use this to obtain all the coins' id in order to make API calls
 * @param {*} include_platform
 * @returns {Promise<object[]>} - List all coins with id, name, and symbol
 */
const getCoinsList = async (include_platform=false) => {
    const params = { include_platform }
    return await rpc('coins/list', params)
}


/**
 * List all supported coins price, market cap, volume, and market related data
 * Use this to obtain all the coins market data (price, market cap, volume)
 *
 * @param vs_currency
 * @param {object} [options]
 * @param {string|number} [options.ids]
 * @param {string|number} [options.category]
 * @param {string|number} [options.order]
 * @param {string|number} [options.per_page]
 * @param {string|number} [options.page]
 * @param {string|number} [options.sparkline]
 * @param {string|number} [options.price_change_percentage]
 * @returns {Promise<object[]>}
 *
 */
const getCoinsMarket = async (vs_currency='usd', options={}) => {
    const { ids, category, order, per_page, page, sparkline=false, price_change_percentage } = options
    const params = { vs_currency, ids, order, per_page, page, sparkline, price_change_percentage }

    return await rpc('coins/markets', filterparams(params))
}


/**
 * Get current data (name, price, market, ... including exchange tickers) for a coin
 *
 * @param {*} id
 * @param {object} [options]
 * @param {boolean} [options.localization]
 * @param {boolean} [options.tickers]
 * @param {boolean} [options.market_data]
 * @param {boolean} [options.community_data]
 * @param {boolean} [options.developer_data]
 * @param {boolean} [options.sparkline]
 * @returns {Promise<object[]>}
 *
 */
const getCoin = async (id, options={}) => {
    const { localization, tickers, market_data, community_data, developer_data, sparkline } = options
    const params = { localization, tickers, market_data, community_data, developer_data, sparkline }

    return await rpc(`coins/${id}`, filterparams(params))
}


/**
 * Get coin tickers (paginated to 100 items)
 * Ticker is_stale is true when ticker that has not been updated/unchanged from the exchange for a while.
 * Ticker is_anomaly is true if ticker's price is outliered by our system.
 *
 * @param id - coin id (can be obtained from /coins)
 * @param {object} [options]
 * @param {string|number} [options.exchange_ids] - filter results by exchange_ids
 * @param {string|number} [options.include_exchange_logo]
 * @param {string|number} [options.page]
 * @param {string|number} [options.order]
 * @param {string|number} [options.depth]
 * @returns {Promise<object[]>}
 *
 */
const getCoinTickers = async (id, options={}) => {
    const { exchange_ids, include_exchange_logo, page, order, depth } = options
    const params = { exchange_ids, include_exchange_logo, page, order, depth }

    return await rpc(`coins/${id}/tickers`, filterparams(params))
}


/**
 * Get historical data (name, price, market, stats) at a given date for a coin
 *
 * @param {string|number} id - coin id (can be obtained from /coins)
 * @param {string|date} date - The date of data snapshot in dd-mm-yyyy eg. 30-12-2017
 * @param {boolean} localization - Set to false to exclude localized languages in response
 * @returns {Promise<object[]>}
 *
 */
const getCoinHistory = async (id, date, localization) => {
    const params = { date, localization }

    return await rpc(`coins/${id}/history`, filterparams(params))
}


/**
 * Get historical market data include price, market cap, and 24h volume (granularity auto)
 *
 * @param {string|number} id - coin id (can be obtained from /coins)
 * @param {string|date} vs_currency
 * @param {string|date} days
 * @param {string|date} interval
 * @returns {Promise<object[]>}
 *
 */
const getCoinMarketChart = async (id, vs_currency, days, interval) => {
    const params = { vs_currency, days, interval }

    return await rpc(`coins/${id}/market_chart`, filterparams(params))
}


/**
 * Get historical market data include price, market cap, and 24h volume within a range of timestamp (granularity auto)
 *
 * @param {string|number} id - coin id (can be obtained from /coins)
 * @param {string|date} vs_currency - The target currency of market data (usd, eur, jpy, etc.)
 * @param {string|date} from - From date in UNIX Timestamp (eg. 1392577232)
 * @param {string|date} to - To date in UNIX Timestamp (eg. 1422577232)
 * @returns {Promise<object[]>}
 */
const getCoinMarketChartRange = async (id, vs_currency, from, to) => {
    const params = { vs_currency, from, to }

    return await rpc(`coins/${id}/market_chart/range`, filterparams(params))
}


/**
 * Get coin's OHLC
 *
 * @param {*} id
 * @param {*} vs_currency - The target currency of market data (usd, eur, jpy, etc.)
 * @param {*} days - Data up to number of days ago (1/7/14/30/90/180/365/max)
 * @returns {Promise<object[]>}
 */
const getCoinOHLC = async (id, vs_currency, days) => {
    const params = { vs_currency, days }

    return await rpc(`coins/${id}/ohlc`, filterparams(params))
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        getCoinsList,
        getCoinsMarket,
        getCoin,
        getCoinTickers,
        getCoinHistory,
        getCoinMarketChart,
        getCoinMarketChartRange,
        getCoinOHLC,
    }
}

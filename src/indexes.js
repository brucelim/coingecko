const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * List all market indexes
 * 
 * @param {object} [options]
 * @param {string|number} [options.include_market_cap]
 * @param {string|number} [options.include_24hr_vol]
 * @returns {Promise<object[]>}
 */
const listIndexes = async (options={}) => {
    const { per_page, page } = options
    const params = { per_page, page }

    return await rpc(`indexes`, filterparams(params))
}


/**
 * get market index
 * 
 * @param {string|number} market_id
 * @param {string|number} id
 * @returns {Promise<object[]>}
 */
const getMarketIndex = async (market_id, id) => {
    const params = { market_id, id }

    return await rpc(`indexes/${market_id}/${id}`, filterparams(params))
}


/**
 * list market indexes id and name
 * 
 * @returns {Promise<object[]>}
 */
const listMarketIndexes = async () => {
    return await rpc(`indexes/list`)
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        listIndexes,
        getMarketIndex,
        listMarketIndexes,
    }
}

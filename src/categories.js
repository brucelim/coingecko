const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * List all categories
 * @returns {Promise<object[]>}
 */
const getCategories = async () => {
    return await rpc(`coins/categories/list`)
}


/**
 * List all categories with market data
 * 
 * @param {*} order
 * @returns {Promise<object[]>}
 */
const getCategoriesWithMarket = async (order) => {
    const params = { order }
    return await rpc(`coins/categories`, filterparams(params))
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        getCategories,
        getCategoriesWithMarket
    }
}

const rpc = require('./rpc')
const filterparams = require('./filter')

/**
 * List all supported NFT ids, paginated by 100 items per page, paginated to 100 items
 * 
 * @param {object} [options]
 * @param {string|number} [options.order]
 * @param {string|number} [options.per_page]
 * @param {string|number} [options.page]
 * @returns {Promise<object[]>}
 */
const listNFT = async (options={}) => {
    const { order, asset_platform_id, per_page, page } = options
    const params = { order, asset_platform_id, per_page, page }

    return await rpc(`nfts/list`, filterparams(params))
}


/**
 * Get current data (name, price_floor, volume_24h ...) for an NFT collection
 * 
 * @param {*} id - id of nft collection (can be obtained from /nfts/list)
 * @returns {Promise<object[]>}
 */
const getNFTCollection = async (id) => {
    return await rpc(`nfts/${id}`)
}


/**
 * Get current data (name, price_floor, volume_24h ...) for an NFT collection.
 * 
 * @param {*} asset_platform_id
 * @param {*} contract_address
 * @returns {Promise<object[]>}
 */
const listNFTbyPlatformContractAddress = async (asset_platform_id, contract_address) => {
    return await rpc(`nfts/${asset_platform_id}/contract/${contract_address}`)
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        listNFT,
        getNFTCollection,
        listNFTbyPlatformContractAddress,
    }
}

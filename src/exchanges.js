const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * List all exchanges (Active with trading volumes)
 * 
 * @param {string|number} per_page
 * @param {string|number} page
 * @returns {Promise<object[]>}
 */
const listExchanges = async (per_page, page) => {
    const params = { per_page, page }
    return await rpc(`exchanges`, filterparams(params))
}


/**
 * List all supported markets id and name (no pagination required)
 * 
 * @returns {Promise<object[]>}
 */
const listSupportedMarkets = async () => {
    return await rpc(`exchanges/list`)
}


/**
 * Get exchange volume in BTC and top 100 tickers only
 * 
 * @param {string|number} id
 * @returns {Promise<object[]>}
 */
const getExchangeVolume = async (id) => {
    return await rpc(`exchanges/${id}`)
}


/**
 * Get exchange tickers (paginated, 100 tickers per page)
 * 
 * @param {string|number} id
 * @param {object} [options]
 * @param {string|number} [options.coin_ids]
 * @param {string|number} [options.include_exchange_logo]
 * @param {string|number} [options.page]
 * @param {string|number} [options.depth]
 * @param {string|number} [options.order]
 * @returns {Promise<object[]>}
 */
const getExchangeTickers = async (id, options={}) => {
    const { coin_ids, include_exchange_logo, page, depth, order } = options
    const params = { coin_ids, include_exchange_logo, page, depth, order }

    return await rpc(`exchanges/${id}/tickers`, filterparams(params))
}


/**
 * Get volume_chart data (in BTC) for a given exchange
 * 
 * @param {string|number} id
 * @param {string|number} days - Data up to number of days ago (eg. 1,14,30)
 * @returns {Promise<object[]>}
 */
const getExchangeVolumeChart = async (id, days) => {
    const params = { days }

    return await rpc(`exchanges/${id}/volume_chart`, filterparams(params))
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        listExchanges,
        listSupportedMarkets,
        getExchangeVolume,
        getExchangeTickers,
        getExchangeVolumeChart,
    }
}

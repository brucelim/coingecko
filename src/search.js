const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * Search for coins, categories and markets on CoinGecko
 * 
 * @param query - Search string
 * @returns {Promise<object[]>}
 */
const search = async (query) => {
    const params = { query }

    return await rpc(`search`, filterparams(params))
}


/**
 * Get trending search coins (Top-7) on CoinGecko in the last 24 hours
 * 
 * @returns {Promise<object[]>}
 */
const trending = async () => {
    return await rpc(`search/trending`)
}

let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        search,
        trending,
    }
}

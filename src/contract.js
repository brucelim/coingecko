const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * Get coin info from contract address
 * @todo sanitize??
 * 
 * @param {*} id - Asset platform (See asset_platforms endpoint for list of options)
 * @param {*} contract_address - Token's contract address
 * @returns {Promise<object[]>}
 */
const getCoinInfo = async (id, contract_address) => {
    return await rpc(`coins/${id}/contract/${contract_address}`)
}


/**
 * Get historical market data include price, market cap, and 24h volume (granularity auto) from a contract address
 * 
 * @param {*} id - Asset platform (See asset_platforms endpoint for list of options)
 * @param {*} contract_address - Token's contract address
 * @param {*} vs_currency - The target currency of market data (usd, eur, jpy, etc.)
 * @param {*} days - Data up to number of days ago (eg. 1,14,30,max)
 * @returns {Promise<object[]>}
 */
const getMarketChart = async (id, contract_address, vs_currency, days) => {
    const params = { vs_currency, days }

    return await rpc(`coins/${id}/contract/${contract_address}/market_chart/`, filterparams(params))
}


/**
 * Get historical market data include price, market cap, and 24h volume within a range of timestamp (granularity auto) from a contract address
 * 
 * @param {*} id - Asset platform (See asset_platforms endpoint for list of options)
 * @param {*} contract_address - Token's contract address
 * @param {string|date} vs_currency - The target currency of market data (usd, eur, jpy, etc.)
 * @param {string|date} from - From date in UNIX Timestamp (eg. 1392577232)
 * @param {string|date} to - To date in UNIX Timestamp (eg. 1422577232)
 * @returns {Promise<object[]>}
 */
const getMarketChartRange = async (id, contract_address, vs_currency, from , to) => {
    const params = { vs_currency, from, to }

    return await rpc(`coins/${id}/contract/${contract_address}/market_chart/range`, filterparams(params))
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        getCoinInfo,
        getMarketChart,
        getMarketChartRange,
    }
}

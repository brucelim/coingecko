const rpc = require('./rpc')
const filterparams = require('./filter')



/**
 * @todo
 * test parse stringify performance vs .fromEntries(.entries)
 *
 * Get the current price of any cryptocurrencies in any other supported currencies that you need.
 * @param {*} ids
 * @param {*} vs_currencies
 * @param {object} [options]
 * @param {string|number} [options.include_market_cap]
 * @param {string|number} [options.include_24hr_vol]
 * @param {string|number} [options.include_24hr_change]
 * @param {string|number} [options.include_last_updated_at]
 * @param {string|number} [options.precision]
 * @returns {Promise<object[]>} - price(s) of cryptocurrency
 */
const getPrice = async (ids, vs_currencies='usd', options={}) => {
    const { include_market_cap, include_24hr_vol, include_24hr_change, include_last_updated_at, precision } = options
    const params = { ids, vs_currencies, include_market_cap, include_24hr_vol, include_24hr_change, include_last_updated_at, precision }

    return await rpc('simple/price', filterparams(params))
}


/**
 * Get current price of tokens (using contract addresses) for a given platform in any other currency that you need.
 * @param {*} id 
 * @param {*} contract_addresses 
 * @param {*} vs_currencies 
 * @param {*} options 
 * @returns {Promise<object[]>} - price(s) of cryptocurrency
 */
const getTokenPrice = async (id, contract_addresses, vs_currencies='usd', options={}) => {
    const { include_market_cap,
        include_24hr_vol,
        include_24hr_change,
        include_last_updated_at,
        precision,
    } = options

    const params = {
        contract_addresses,
        vs_currencies,
        include_market_cap,
        include_24hr_vol,
        include_24hr_change,
        include_last_updated_at,
        precision,
    };

    return await rpc(`simple/token_price/${id}`, filterparams(params))
}


/**
 * Get a list of supported_vs_currencies that can be used as the
 * `vs_currency` parameter in the `simple/price` endpoint
 * @returns {Promise<object[]>} An array of supported vs currencies
 */
const getSupportedVsCurrencies = async () => {
    return await rpc('simple/supported_vs_currencies')
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        getPrice,
        getTokenPrice,
        getSupportedVsCurrencies,
    }
}

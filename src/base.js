const rpc = require('./rpc')
const filterparams = require('./filter')


/**
 * Check API server status.
 * @returns Status Ok
 */
const ping = async () => {
    const res = await rpc('ping')
    return res;
}


/**
 * List all asset platforms (Blockchain networks)
 * 
 * @param {*} filter - apply relevant filters to results valid values: "nft" (asset_platform nft-support)
 * @returns {Promise<object[]>}
 * 
 */
const getAssetPlatforms = async (filter) => {
    const params = { filter }

    return await rpc(`asset_platforms`, filterparams(params))
}


/**
 * Get BTC-to-Currency exchange rates
 * 
 * @returns {Promise<object[]>}
 */
const getExchangeRates = async () => {
    return await rpc(`exchange_rates`)
}


/**
 * Get cryptocurrency global data
 * 
 * @returns {Promise<object[]>}
 */
const getGlobalData = async () => {
    return await rpc(`global`)
}


/**
 * Get cryptocurrency global decentralized finance(defi) data
 * 
 * @returns {Promise<object[]>}
 */
const getGlobalDefiData = async () => {
    return await rpc(`global/decentralized_finance_defi`)
}


/**
 * Get public companies data
 */
const getPublicCompanyData = async (coin_id) => {
    return await rpc(`companies/public_treasury/${coin_id}`)
}


let apikey;
module.exports = function(apikey) {
    apikey = apikey;

    return {
        ping,
        getAssetPlatforms,
        getExchangeRates,
        getGlobalData,
        getGlobalDefiData,
        getPublicCompanyData,
    }
}

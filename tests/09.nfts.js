const test = require('tape');
const { nfts } = require('../index')


test('[09-00] nfts module setup', (t) => {
    t.ok(nfts)
    t.is(typeof nfts, 'object')
    t.end()
})


test('[09-01] /nfts/list', async (t) => {
    const list = await nfts.listNFT()
    
    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
})


// { error: 'nft collection not found' }
test('[09-02] /nfts/{id}', async (t) => {
    const id = 'cryptopunks'
    const collection = await nfts.getNFTCollection(id)

    t.ok(collection)
    t.is(collection.constructor, Object.prototype.constructor)
    t.is(collection.id, id)
})


test('[09-03] /nfts/{asset_platform_id}/contract/{contract_address}', async (t) => {
    const asset_platform = 'ethereum'
    const contract_address = '0xb47e3cd837dDF8e4c57F05d70Ab865de6e193BBB'

    const collection = await nfts.listNFTbyPlatformContractAddress(asset_platform, contract_address)
  
    t.ok(collection)
    t.is(collection.constructor, Object.prototype.constructor)
    t.is(collection.contract_address, contract_address)
})

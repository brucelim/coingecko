const test = require('tape');
const { indexes } = require('../index')


test('[07-00] indexes module setup', (t) => {
    t.ok(indexes)
    t.is(typeof indexes, 'object')
    t.end()
})


test('[06-01] /indexes', async (t) => {
    const list = await indexes.listIndexes()

    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
})


// { error: 'Could not find market with the given params' }
test('[06-02] /indexes/{market_id}/{id}', async (t) => {
    const market_id = 'bitmart'
    const id = 'bitcoin'
    const missing_index = await indexes.getMarketIndex(market_id, id)

    t.ok(missing_index)
    t.is(missing_index.constructor, Object.prototype.constructor)
    t.is(missing_index.error, 'Could not find market with the given params')
})


test('[06-03] /indexes/list', async (t) => {
    const market_indexes = await indexes.listMarketIndexes()

    t.ok(market_indexes)
    t.is(market_indexes.constructor, Array.prototype.constructor)
})

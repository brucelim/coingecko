const test = require('tape');
const { search } = require('../index')


test('[11-00] search module setup', (t) => {
    t.ok(search)
    t.is(typeof search, 'object')
    t.end()
})


test('[11-01] /search', async (t) => {
    const results = await search.search('bitcoin')

    t.ok(results)
    t.is(results.constructor, Object.prototype.constructor)
    t.is(results.coins.constructor, Array.prototype.constructor)
})


test('[11-02] /search/trending', async (t) => {
    const results = await search.trending()

    t.ok(results)
    t.is(results.constructor, Object.prototype.constructor)
    t.is(results.coins.constructor, Array.prototype.constructor)
    t.is(results.coins.length, 7)
})

const test = require('tape');
const { getPublicCompanyData } = require('../index')

// ReferenceError: AsyncFunction is not defined node 16
test('[13-00] company functions are available', (t) => {
    t.ok(getPublicCompanyData)
    t.is(typeof getPublicCompanyData, 'function')

    t.end()
})


test('[13-01] /companies/public_treasury/{coin_id}', async (t) => {
    const coin_id = 'bitcoin'
    const holdings = await getPublicCompanyData(coin_id)

    t.ok(holdings)
    t.is(holdings.constructor, Object.prototype.constructor)
    t.is(holdings.companies.constructor, Array.prototype.constructor)
})


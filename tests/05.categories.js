const test = require('tape');
const { categories } = require('../index')


test('[05-00] categories module setup', (t) => {
    t.ok(categories)
    t.is(typeof categories, 'object')
    t.end()
})


test('[05-01] /coins/categories/list', async (t) => {
    const list = await categories.getCategories()
    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
})


test('[05-02] /coins/categories', async (t) => {
    const categories_with_market = await categories.getCategoriesWithMarket()

    t.ok(categories_with_market)
    t.is(categories_with_market.constructor, Array.prototype.constructor)
})

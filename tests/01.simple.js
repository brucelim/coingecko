const test = require('tape');
const { simple } = require('../index')


test('[01-00] coingecko module setup', (t) => {
    t.ok(simple)
    t.is(typeof simple, 'object')
    t.end()
})

test('[01-01] /simple/price', async (t) => {
    const ids = 'hex'
    const price = await simple.getPrice('hex')

    t.ok(price)
    t.is(typeof price[ids], 'object')
    t.end()
})

// { error: 'asset platform not found' }
test('[01-02] /simple/token_price', async (t) => {
    const ids = 'ethereum'
    const contract_address = '0x2b591e99afE9f32eAA6214f7B7629768c40Eeb39'
    const vs_currency = 'eth'
    const tokenprice = await simple.getTokenPrice(ids, contract_address, vs_currency)

    t.ok(tokenprice)
    t.is(tokenprice.constructor, Object.prototype.constructor)
    t.is(Object.keys(tokenprice)[0].toLowerCase(), contract_address.toLowerCase())
})

test('[01-03] /simple/supported_vs_currencies', async (t) => {
    const currencies = await simple.getSupportedVsCurrencies()

    t.ok(currencies)
    t.is(currencies.constructor, Array.prototype.constructor)
    t.is(currencies.some(e=>e==='btc'), true)
    t.end()
})

const test = require('tape');
const { contract } = require('../index')


test('[03-00] contract module setup', (t) => {
    t.ok(contract)
    t.is(typeof contract, 'object')
    t.end()
})


test('[03-01] /coins/{id}/contract/{contract_address}', async (t) => {
    const id = 'hex'
    const contract_address = '0x2b591e99afE9f32eAA6214f7B7629768c40Eeb39'
    const token = await contract.getCoinInfo(id, contract_address)

    t.ok(token)
    t.is(token.id, id)
    t.is(token.platforms.ethereum.toLowerCase(), contract_address.toLowerCase())
})


test('[03-02] /coins/{id}/contract/{contract_address}/market_chart/', async (t) => {
    const id = 'hex'
    const contract_address = '0x2b591e99afE9f32eAA6214f7B7629768c40Eeb39'
    const vs_currency = 'usd'
    const days = 1
    const market = await contract.getMarketChart(id, contract_address, vs_currency, days)

    t.ok(market)
    t.is(market.constructor, Object.prototype.constructor)
    t.isNot(Object.keys(market).indexOf('prices'), -1)
    t.is(market.prices.length, 289)
})


test('[03-03] /coins/{id}/contract/{contract_address}/market_chart/range', async (t) => {
    const id = 'hex'
    const contract_address = '0x2b591e99afE9f32eAA6214f7B7629768c40Eeb39'
    const vs_currency = 'usd'
    const from = 1606960228
    const to = 1607046628
    
    const marketrange = await contract.getMarketChartRange(id, contract_address, vs_currency, from, to)

    t.ok(marketrange)
    t.is(marketrange.constructor, Object.prototype.constructor)
    t.isNot(Object.keys(marketrange).indexOf('prices'), -1)
    t.is(marketrange.prices.length, 24)
})

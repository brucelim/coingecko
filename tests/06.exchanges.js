const test = require('tape');
const { exchanges } = require('../index')


test('[06-00] exchanges module setup', (t) => {
    t.ok(exchanges)
    t.is(typeof exchanges, 'object')
    t.end()
})


test('[06-01] /exchanges', async (t) => {
    const list = await exchanges.listExchanges()

    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
})


test('[06-02] /exchanges/list', async (t) => {
    const supported_markets = await exchanges.listSupportedMarkets()

    t.ok(supported_markets)
    t.is(supported_markets.constructor, Array.prototype.constructor)
})


test('[06-03] /exchanges/{id}', async (t) => {
    const id = 'bitmart'
    const volume = await exchanges.getExchangeVolume(id)

    t.ok(volume)
    t.is(volume.constructor, Object.prototype.constructor)
    t.is(volume.name.toLowerCase(), id)
})


test('[06-04] /exchanges/{id}/tickers', async (t) => {
    const id = 'bitmart'
    const tickers = await exchanges.getExchangeTickers(id)

    t.ok(tickers)
    t.is(tickers.constructor, Object.prototype.constructor)
    t.is(tickers.name.toLowerCase(), id)
    t.is(tickers.tickers.constructor, Array.prototype.constructor)
})


test('[06-04] /exchanges/{id}/volume_chart', async (t) => {
    const id = 'bitmart'
    const days = 1
    const volume_chart = await exchanges.getExchangeVolumeChart(id, 1)

    t.ok(volume_chart)
    t.is(volume_chart.constructor, Array.prototype.constructor)
    t.is(volume_chart[0].length, 2)
})


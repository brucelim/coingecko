const test = require('tape');
const { getGlobalData, getGlobalDefiData } = require('../index')

// ReferenceError: AsyncFunction is not defined node 16
test('[12-00] global functions are available', (t) => {
    t.ok(getGlobalData)
    t.is(typeof getGlobalData, 'function')

    t.ok(getGlobalDefiData)
    t.is(typeof getGlobalDefiData, 'function')

    t.end()
})


test('[12-01] /global', async (t) => {
    const data = await getGlobalData()

    t.ok(data)
    t.is(data.constructor, Object.prototype.constructor)
    t.is(data.data.total_market_cap.constructor, Object.prototype.constructor)
})


test('[12-02] /global/decentralized_finance_defi', async (t) => {
    const data = await getGlobalDefiData()

    t.ok(data)
    t.is(data.constructor, Object.prototype.constructor)
    t.is(data.data.constructor, Object.prototype.constructor)
})

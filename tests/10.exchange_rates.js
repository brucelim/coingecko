const test = require('tape');
const { getExchangeRates } = require('../index')

// ReferenceError: AsyncFunction is not defined node 16
test('[10-00] asset_platform is available', (t) => {
    t.ok(getExchangeRates)
    t.is(typeof getExchangeRates, 'function')

    t.end()
})

test('[10-01] /exchange_rates', async (t) => {
    const exchange_rates = await getExchangeRates()

    t.ok(exchange_rates)
    t.is(exchange_rates.constructor, Object.prototype.constructor)
    t.is(exchange_rates.rates.btc.value, 1)
})

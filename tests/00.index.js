const test = require('tape');
const Coingecko = require('../index')
const coingecko = new Coingecko()


test('[00] coingecko module setup', async (t) => {
    t.ok(Coingecko.simple)
    t.ok(coingecko.ping)
    t.ok(coingecko.simple)

    const ping = await coingecko.ping()
    t.ok(ping)
    t.is(typeof ping, 'object')
    t.is(ping.gecko_says, '(V3) To the Moon!')

    t.end()
})

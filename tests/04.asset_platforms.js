const test = require('tape');
const coingecko = require('../index')
const { getAssetPlatforms } = require('../index')


// ReferenceError: AsyncFunction is not defined node 16
test('[04-00] asset_platform is available', (t) => {
    t.ok(getAssetPlatforms)
    t.is(typeof getAssetPlatforms, 'function')
    // t.is(getAssetPlatforms.constructor, Function.prototype.constructor)

    t.end()
})

test('[04-01] /asset_platforms', async (t) => {
    const platforms = await getAssetPlatforms()
    t.ok(platforms)
    t.is(platforms.constructor, Array.prototype.constructor)

    const ethereum = platforms.filter(chain => {
        return chain.id == 'ethereum'
    }).shift()

    t.ok(ethereum)
    t.is(ethereum.constructor, Object.prototype.constructor)
    t.is(ethereum.chain_identifier, 1)
})

const test = require('tape');
const { derivatives } = require('../index')


test('[08-00] derivatives module setup', (t) => {
    t.ok(derivatives)
    t.is(typeof derivatives, 'object')
    t.end()
})


test('[08-01] /derivatives', async (t) => {
    const derivative_tickers = await derivatives.listDerivativeTickers()
    
    t.ok(derivative_tickers)
    t.is(derivative_tickers.constructor, Array.prototype.constructor)
})


test('[08-02] /derivatives/exchanges', async (t) => {
    const derivative_exchanges = await derivatives.listDerivativeExchanges()

    t.ok(derivative_exchanges)
    t.is(derivative_exchanges.constructor, Array.prototype.constructor)
})


test('[08-03] /derivatives/exchanges/{id}', async (t) => {
    const id = 'binance_futures'
    const exchange = await derivatives.getDerivativeExchange(id)

    t.ok(exchange)
    t.is(exchange.constructor, Object.prototype.constructor)
    t.is(exchange.url, 'https://www.binance.com/')
})


test('[08-04] /derivatives/exchanges/list', async (t) => {
    const list = await derivatives.listDerivativeExchangesNameIdentifier()

    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
})

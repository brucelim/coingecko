const test = require('tape');
const { coins } = require('../index')


test('[02-00] coingecko module setup', (t) => {
    t.ok(coins)
    t.is(typeof coins, 'object')
    t.end()
})

test('[02-01] coins/list', async (t) => {
    const list = await coins.getCoinsList()

    t.ok(list)
    t.is(list.constructor, Array.prototype.constructor)
    t.end()
})

test('[02-02] coins/market', async (t) => {
    const markets = await coins.getCoinsMarket()

    t.ok(markets)
    t.is(markets.constructor, Array.prototype.constructor)
    // @todo check for some market entries

    t.end()
})

test('[02-03 coins/{id}]', async (t) => {
    const missing = 'notfound'
    const notfound = await coins.getCoin(missing)

    t.ok(notfound)
    t.is(notfound.error, 'coin not found')

    const id = 'bitcoin'
    const coin = await coins.getCoin(id)

    t.ok(coin)
    t.is(coin.id, id)
})


test('[02-04] /coins/{id}/tickers', async (t) => {
    const id = 'bitcoin'
    const tickers = await coins.getCoinTickers(id)

    t.ok(tickers)
    t.is(tickers.name.toLowerCase(), id)
    t.is(tickers.tickers.length, 100)
})


test('[02-05] /coins/{id}/history', async (t) => {
    const id = 'bitcoin'
    const date = '30-12-2017'
    const history = await coins.getCoinHistory(id, date)

    t.ok(history)
    t.is(history.id, id)
    t.is(history.market_data.current_price.usd, 13620.3618741461)
})


test('[02-06] /coins/{id}/market_chart', async (t) => {
    const id = 'bitcoin'
    const vs_currencies = 'usd'
    const days = 30
    const data = await coins.getCoinMarketChart(id, vs_currencies, days)

    t.ok(data)
    t.isNot(Object.keys(data).indexOf('prices'), -1)
})


test('[02-07] /coins/{id}/market_chart/range', async (t) => {
    const id = 'bitcoin'
    const vs_currency = 'usd'
    const from = 1392577232
    const to = 1392663632

    const data = await coins.getCoinMarketChartRange(id, vs_currency, from, to)

    t.ok(data)
    t.isNot(Object.keys(data).indexOf('prices'), -1)
    t.is(data['prices'][0][0], 1392595200000)
    t.is(data['prices'][0][1], 645.14)
})


test('[02-08] /coins/{id}/ohlc', async (t) => {
    const id = 'bitcoin'
    const vs_currency = 'usd'
    const days = 7

    const data = await coins.getCoinOHLC(id, vs_currency, days)

    t.ok(data)
    t.is(data.constructor, Array.prototype.constructor)
    t.is(data.length, 43)
})
